### Google BigTable
- What is Google BigTable?
Created on Feburary 2005, Google BigTable stores data such as time-series, marketing, and financial data. It is a NoSQL database service. It uses this with Google BigTable because of their shared ability to work with large data sets. 
NoSQL databases are designed for distributed data stores that have extremely large data storage needs.

- What is it used for?

Google big table is used to store and work with large amounts of data.  It uses NoSQL to work with large amounts of data and build responsive applications. Ideal uses for things like real time analytics.   
It is used for low-latancy access, being quick access to big storage/data. It is a good solution for minimum 1 terabyte of data but anything less won't be helpful.

### Python/ML

- What is python/ml?

ML stands for machine learning and is a subset of AI. ML is used for datasets. Machine learning can be used by most scriping languages but Python is considered the best fit for it. 

- What is it used for?

ML is used to analyze data and to predict the outcome. It can be used to speed up processes such as coding or even regular search engines. This is used for websites in recomendations based off recent searches or even voice recognition. Python is used in machine learning due to its easy to use and learn nature. 

- Why is Python the best for ML?

Python is the best for ML because of its Simplicity and consistency. ML is very complicated to use and Python simplifies it for general usage. ML recognizes patterns in your data and having consistancy and easy to read patterns makes the experience much easier for yourself and the machine.  

Python also has a large number of libraries at its disposal. Some good examples for ML would be :

- scikit-learn 
- Tensorflow
- pylearn2 
Which all can be used for data mining and machine learning.

### Amazon S3

- What is Amazon S3?

Launched on March 14, 2006, Amazon S3 is a service that provides object storage through a web service interface.  Amazon S3 uses the same scalable storage infrastructure that Amazon.com uses to run its e-commerce network.  The S3 stands for Amazon Simple Storage Service.

- What is it used for?

Amazon S3 is used on the same scalable storage infrastructure Amazon uses. It can store and retrieve any amount anytime and anywhere.

It is great for moving and storing data and monitoring data at object and bucket levels. 
Bucket levels refers to a fundamental logical container where data is stored in Amazon S3 storage. You can store an infinite amount of data and unlimited number of objects in a bucket.  All of amazon S3's data is stored in these.

A max amount of 100 can be created with regular service. 



