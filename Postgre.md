### Various Postgre commands 
- create and drop table
CREATE TABLE tutorials (id int, tutorial_name text);
Attempting to create the same table will result in an error. 
If the command IF NOT EXISTS is used, you will get a notice instead of an error.
- PostGre drop table
DROP TABLE table_name;
