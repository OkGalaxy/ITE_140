### What is Postgre?


- Postgre is a free open sourced database management tool.
The project was lead by Michael Stonebreaker, a pioneer of database research technology. He started the project that is know today as a Postgre which got its name from the older database, Ingres. 

The purpose of ProgresSQL was to create a service that had the fewest features required to support large databases.

- Popular comapnies and applications like Uber, Instagram and Netflix all reportedly use PostgreSQL within their tech stack. 



### Who is Michael Stonebraker
- Michael Stonebraker is a computer science specialist who over the years, worked to contribute large projects and works to the computer science and programming industry. 

-  Michael is most famous for his creation of INGRES, which is an SQL relational database management system. He is the co creator of the PostgreSQL system which was deemed as the sucessor to INGRES. The name itself is refering to post INGRES which is why it is called Postgre.


### History of Postgre

- POSTGRES was implemented in 1986 which was a project lead by Michael Stonebraker. The PostgreSQL database management system is derrived from the POSTGRES package written at the University of California.

- PostgreSQL is written in C which is a general purpose computer programming language that was created in 1970 which is around the time home computers were starting to be invented by companies like Apple

- The project known as PostgreSQL, which is the name widely used today, originally was called POSTGRES (with all capitals) as a start. It was changed in 1996 to reflect its support for SQL, which is what it is widely used for today. 


### PostgreSQl presense today

- PostgreSQl is still widely used today as it remains a free software for anyone even with commercial usage. Many companies take advantage of this and use it for database management within their own servers 

- PostgreSQL is still being actively developed and worked on to this day and recieves changes periodically. Their website depicts these changes and gives meaningful feedback and answers questions that users may have. Their website also details a brief history about the project and its commitment to maining it as a free software. 