#Impact of Electronic Gadget on students Results
This dataset revolves around the amount of gadgets a student would have (which we would just call computers) in relation to their overall preformance in schools. 
It was taken to see if students with higher technology usage would experience a negative effect on their grades.
### Attributes of the dataset
- your Gender: gender of the students
- Study Hour: Study hour done by the students
- Your Sleep Hour: Total sleep in a day by the student
- Weekend: Was it a weekend in the place of the student
- What is your age: Age of the students
- what is your cgpa: Current CGPA of the student
# Results of data
Based on the data from the sets, It was all usually mixed whether or not a student did well with having a lot of electronics or not. 
[Dataset_code](https://www.kaggle.com/code/usmanjon/eda-gadgets/notebook)
![Pie chart](https://www.kaggleusercontent.com/kf/106017750/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..rsSFiMnVUgUJ7Tv_g-FADg.H3tE7jSBavnePaos0LM1kDOJCzdoA2DVHp9ECKta3b4waqq7kR0SZ67sUMY07QPGlvhvD6itGiDtNl21BRNqsMBaX7deDt0GTBJIGCIemXEmF1W5ZY8GkNegQHjOJ1nrdQR06Xcg03WX10cz-LnsUx3nTIOs0AO2I50hj2oO_KYPBcp3f_dSOySaKCEgE6Wb2En6mk9JepkshwSToxjcW9Haz6bUphm07D2tZu8pdHwHSGd5zCcAWIWBf3vSGOELpYoN8rBc_Qd-fgJ8vjo0wY53F7dPcqji97NvQqE01XGowWdAzA5b5a9nZmwFtn3kxhRSNLQ5TRBZfKIc9uDigLjmGmMapooKxgIUsGL34ldimpqEfKsPKwEMJOQ6sUrIkF43U5H0eeXLGTqkQFNNioPmfA0yiFFYB2awHdqCr4XaqUg4kyZ06A_BueXRq_4PtRqcenRoNjeNouTTUrZIu5QNApWE1O2dhdSoMp7su_ap_aC7VN0xukCuMemha1yFt9nq8oDX0x-uA2-js2_oUS9O6H6ZyVfJwacO-jbBnHXDeAspzApmnWK1yqocV1wWI-X4wx1bubso7etz3c7VIWE8AbLtkEJAD5HGWpUutMi8mGwUAXr8WnqSAAViicMjCpmKBOdY3j_APtfUa3xcnQ.gT5JMB4lWUSW4_DGSPVHxw/__results___files/__results___9_0.png)
### Additonal investigation
# Where did the data come from?
- The data was taken from students in college
- It represents their cgpa  and amount of gadgets

#  How was the data gathered?
The data was gathered through the use of student records. It took into account of their major and the amount of gadgets
It was created to make predictions on whether or not the student cgpa was affected by their use of gadgets.

# Who owns the data?

Based on the description, I would assume that the Department of Information and Communication Technology are the people that own the data. The rights and License is listed as other, but when looking in the description, there is no other listed information. 
