# What is pandas
- Pandas is a Python package used data manipulation and analysis that offers data structures and operations for manipulating data.
- Within python it contains many different functionalities but its main functions are data allignment and verctorization. It contains a lot of other tasks like working with statistics, time series, and interactive charts. 
- Pandas is built on top of another package called NumPy, which for built for data structures like arrays and NumPy is considered the backbone of Pandas.
# What is pandas used for?
- Pandas is widely used as an interface to get data into and out of spreadsheets. It is used to create datastructures that is labeled within arrays. 

- Pandas main functions like **Dataframe** and **Series** are used to make these one and two dimensional arrays. It was created 
to analyze data but is build around/upon of other Python packages like NumPy.
# Advantages of Pandas 
- It is fast and efficient for manipulating and analyzing data, handing for missing data for floating point and non floating point data. Merging and joining of datasets. Easy time series manipulation. Easy data set manipulation.
-  For example, things like  Dataframe creates two-dimensional arrays like NumPy but it comes with column and row labels and each column can hold different data types. Series is similar to one-dimensional arrays in NumPy but with labels. 