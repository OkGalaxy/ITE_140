# NumPy array
 A one dimension array contains only one axis. For example >1 by 3. A two dimensional array would have two axis.
 If an array consisted of only numbers but one float, its dtype will be float64 always. You can test the array type by inputing 'array1.dtype'. Your ouput in this case would be 'dtype('float64')'
 # Vectorization and Broadcasting
 ### Vectorization
 Building the sum of the scalar and a NumPy array will preform an element wise opperation. It will add the values across the entire array you specified. A good example code for this would be 'array2 + 1'. Your output would be the orignal array but each value has one added to it.
 This applys to multiplication as well. 'array2 * array2' *this out put would multiply each data in the array by the matching in the 2nd.*
### Brodcasting
Using two arrays with different shapes will cause python to extend the smaller array across the larger one so that the shapes can be compatible. *if possible.* Matrix multiplicatin or dot products are preformed using the @ operand. 
# Universal functions
Universal functions are functions that work on every element of a NumPy array. If you were to use a function like square root from python's 'import math', you would recieve an error. If necessary you can use a nested loop to get the square root from every element and create an array from the result. It is more efficient to use a unfunc if it's availible. 'array2.sum(axis=0)'
will give u the sum of the axis along the rows. axis=(1) will preform the same thing along columns. You can leave out the axis part if you want the sum of the entire array.
# Creating and Manipulating Arrays
## Getting and selecting array elements 
numpy_array[row_selection, column_selection]
1 dimensional arrays don't require the 'column_selection' part. To index an array you would need to enter a start and an end. start:end. Entering nothing will display the entire array. The colon represents continuation so for example [: ,1:] would contain both rows but start at column 1 and continue. Displaying everything but the 0 column.
# Definitions 
 Scalar
 : Scalar refers to a single number. Vector would be an array. Scalar is used as a data type that consists of both floats and arrays.

