import numpy as np 
import pandas as pd 
import os
df = pd.DataFrame()
for dirname, _, filenames in os.walk('/kaggle/input'):
    for filename in filenames:
        print(os.path.join(dirname, filename)) 
data=pd.read_csv('GadgetUsage.csv')
data.head()
from datetime import datetime
import time
for i in range(len(data["Timestamp"])):
    data["Timestamp"].loc[i]
    date=time.mktime(datetime.strptime(data["Timestamp"].loc[i], "%m/%d/%Y %H:%M:%S").timetuple())
    data["Timestamp"].loc[i]=date
data.head()
data.isnull().sum()

from sklearn.preprocessing import LabelEncoder
 

En = LabelEncoder()
 

Enco = En.fit_transform(data['Gender'])
data.drop("Gender", axis=1, inplace=True)
 

data["Gender"] = Enco

data["Your study year"].value_counts()

data["Your study year"]=data["Your study year"].str.replace(r"Post Graduate","4")
data["Your study year"].dtypes

from sklearn.preprocessing import LabelEncoder
 

L = LabelEncoder()
 

En = L.fit_transform(data['Which department'])
data.drop("Which department", axis=1, inplace=True)
 

data["Which department"] = En

data.head()

data["Is there an increase in your CGPA?"].value_counts()
df = pd.DataFrame(data)
print(df)