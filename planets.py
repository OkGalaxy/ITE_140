import pandas as pd
import numpy as np

data=pd.read_csv('planets.csv')
data = [["Mercury", "Grey", 0.330, "No", "Yes"], ["Venus", "Brown and Grey", 4.870, "No", "Yes"]]
df = pd.DataFrame(data=data,
                          columns=["name", "Color", "Mass",
                                   "Ring system?", "Magnetic Field?"],
                          index=[1001, 1000,])
print(df)

print(df.info)

print(df.groupby(["name"]).mean())

print(df.groupby(["name", "Color"]).mean())

selection = df.loc[:, ["name", "Color", "Mass"]]
selection.groupby(["name"]).agg(lambda x: x.max() - x.min())

data = [["Mercury", " Grey", 0.330],
        ["Venus", "Brown and Grey", 4.870],
        ["Earth", "Blue and green",5.97 ],
        ["Mars", "Orange, Brown and Red", 0.642]]
planets = pd.DataFrame(data=data, columns = ["Planet", "Color", "Mass"])
print(planets)

pivot = pd.pivot_table(planets, index="Planet", columns="Color",
                                values="Mass")
print(pivot)

pd.melt(pivot.iloc[:-1,:-1].reset_index(),
                 id_vars="Planet",
                 value_vars=["Color", "Mass"], value_name="Mass")